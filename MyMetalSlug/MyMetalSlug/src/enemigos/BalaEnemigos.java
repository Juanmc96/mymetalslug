package enemigos;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;
import metalslug.JuegoMyMetalSlug;
import metalslug.Solid;
import unJugador.PantallaDeJuego;

public class BalaEnemigos extends BaseActor {
	protected PantallaDeJuego nivel;
	protected float tiempoBala;
	protected float cuentaTiempo;
	protected float velocidadBala;
	protected int direccion;
	protected boolean enabled;

	/**
	 * Inicializo las variables
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public BalaEnemigos(float x, float y, Stage s, PantallaDeJuego nivel) {
		super(x, y, s);

		loadTexture("assets/extra/bala.png");
		tiempoBala = 10;
		this.nivel = nivel;
		velocidadBala = 800;
		enabled = false;
	}

	/**
	 * Indicamos la posicion de la bala en tiempo real
	 */
	@Override
	public void act(float dt) {
		
		if (enabled) {
			super.act(dt);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);// Posici�n de la
															// bala seg�n su
															// velocidad

			
			if (this.enabled && this.overlaps(nivel.jugador)) {// Comprobar si
																// bala
																// colisiona con
																// jugador
				nivel.jugador.dano(6);// Indicamos el da�o que hace
				this.enabled = false;// Quitamos la bala cuando impacta con
										// jugador

			}
			
			if (JuegoMyMetalSlug.jug==2) {
				if (this.enabled && this.overlaps(nivel.jugador2)) {// Comprobar si
					// bala
					// colisiona con
					// jugador
					nivel.jugador2.dano(6);// Indicamos el da�o que hace
					this.enabled = false;// Quitamos la bala cuando impacta con
					// jugador

				}
			}

			cuentaTiempo -= dt;// Calculamos el tiempo que le queda a la bala

			if (cuentaTiempo <= 0) {
				enabled = false;// Si se acaba el tiempo se desactiva la bala
			}

		}
	}

	/**
	 * Pintamos la bala si esta activa
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {

		if (enabled) {
			super.draw(batch, parentAlpha);// Si se activa la bala se pinta
		}
	}

	/**
	 * Se activa la bala y se pinta en la direccion correcta
	 * @param x
	 * @param y
	 * @param posx
	 * @param posy
	 */
	public void disparar(float x, float y, float posx, float posy) {

		this.enabled = true;// Activo la bala

		this.setPosition(posx, posy);// Donde comienza el disparo

		cuentaTiempo = tiempoBala;// Inicio la cuenta de la bala
		this.velocityVec.set(velocidadBala * x, velocidadBala * y);// Direccion
																	// de la
																	// bala
		this.direccion = direccion;
		if (posx < 0) {
			direccion = -1;
		} else {
			direccion = 1;
		}
	}
	
	

}
