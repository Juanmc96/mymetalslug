package enemigos;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;
import metalslug.Cuesta;
import metalslug.Parametros;
import metalslug.Solid;
import unJugador.PantallaDeJuego;

public class Cuchillo extends EnemigoCuchillo {
	
	private int direccion;
	private Animation andando;
	private float velocidadMaxima;
	private float distanciaVision;// La distancio en la que ve a jugador
	private float distanciaVision2;// La distancio en la que ve a jugador
	private float aceleracion;
	private BaseActor pie;
	private float velocidadSalto;
	private boolean enabled;
	private BaseActor SenSuelo;// Indicar los sensores del saltarin del suelo
	private BaseActor SenFrente;// Indicar los sensores del saltarin de la
	
	
	/**
	 * En este metodo iniciamos todas las variables del jugador 2
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public Cuchillo(float x, float y, Stage s, PantallaDeJuego nivel) {
		super(x, y, s, nivel);
		// Animaciones
		//quieto = loadTexture("assets/Enemi/c1.png");
		String[] walkFileNames2 = { "assets/Enemi/c1.png", "assets/Enemi/c2.png",
				"assets/Enemi/c2.png", "assets/Enemi/c4.png", "assets/Enemi/c5.png", "assets/Enemi/c6.png","assets/Enemi/c7.png","assets/Enemi/c8.png", };
		andando = loadAnimationFromFiles(walkFileNames2, 0.1f, true);
		// Como sera el movimiento del saltarin
		//velocidadMaxima = 0;
		aceleracion = 200;
		direccion = 1;
		velocidadSalto = 800;
		distanciaVision = 220;// La distancia de vision
		distanciaVision2 = 10;// La distancia de vision

		// Sendores de saltarin
		SenSuelo = new BaseActor(0, 0, s);
		SenSuelo.setSize(this.getWidth() / 6, 30);
		SenSuelo.setBoundaryRectangle();
		ajustarEnSuelo();

		SenFrente = new BaseActor(0, 0, s);
		SenFrente.setSize(this.getWidth() / 6, 30);
		SenFrente.setBoundaryRectangle();
		
		

	}

	/**
	 * Actualizo la distancia de vision con el jugador y compruebo solidos en tiempo real
	 * ademas compruebo como debe de actuar en cada moemnto
	 */
	@Override
	public void act(float dt) {

		super.act(dt);
		
		accelerationVec.add(0, -Parametros.getGravedad());
		
		boolean pisa = false; // Opcion de si esta o no el sensor del suelo
								// tocando
		boolean SenFrentezo = false;// Si se da de frente o no
		
		
		if (Math.sqrt(Math.pow(this.getX() - nivel.jugador.getX(), 2)
				+ Math.pow(this.getY() - nivel.jugador.getY(), 2)) < distanciaVision) {
			velocidadMaxima = 100;
		}
		if (Math.sqrt(Math.pow(this.getX() - nivel.jugador.getX(), 2)
				+ Math.pow(this.getY() - nivel.jugador.getY(), 2)) < distanciaVision2) {
			velocidadMaxima = 0;
		}
		
		for (Solid solido : nivel.getSolidos()) {
			if (SenSuelo.overlaps(solido)) {
				pisa = true;// Si toca solido el sensor se pone en true
			}
			if (SenFrente.overlaps(solido)) {
				SenFrentezo = true;// Si toca solido el sensor se pone en true
			}

		}
		for (Cuesta cuesta : nivel.getCuestas()) {
			if (SenSuelo.overlaps(cuesta)) {
				pisa = true;// Si toca solido el sensor se pone en true
			}
			if (SenFrente.overlaps(cuesta)) {
				SenFrentezo = true;// Si toca solido el sensor se pone en true
			}

		}

		if (nivel.jugador.getX() < this.getX()) {// Si se da de frente cambia de direccion
			direccion = -1;
		}
		else {
			direccion = 1;
		}
		if (direccion > 0) {
			setScaleX(-1);// La imagen se pone en la direccion correcta
		} else {
			setScaleX(1);// La imagen se pone en la direccion correcta
		}
		
		if (SenFrentezo || !pisa) {// Si se da de frente cambia de direccion
			velocidadMaxima = 0;
		}
		

		moveBy(direccion * velocidadMaxima * dt, velocityVec.y * dt);// Posici�n seg�n su velocidad

		ajustarEnSuelo();
		ajustarEnFrente();
	
	}

	
	/**
	 * Compruebo si esta en solido
	 * @return
	 */
	public boolean isOnSolid() {
		// devuelve 'true' si el personaje est� sobre un s�lido activo
		for (Solid solido : nivel.getSolidos()) {
			if (this.pie.overlaps(solido) && solido.isEnabled()) {
				return true;
			}

		}
		for (Cuesta cuesta : nivel.getCuestas()) {
			if (this.pie.overlaps(cuesta) ) {
				return true;
			}

		}
		return false;

	}
	
	/**
	 * Configuramos como es el salto y cuando se ejecute
	 * este metodo hace un salto
	 */
	public void salto() {
		velocityVec.y = velocidadSalto;
		velocityVec.x = direccion * velocidadSalto / 5;
		this.setAnimation(andando);
	}

	/**
	 * Se colocan los sensores en su sitio segun la direccion
	 */
	private void ajustarEnFrente() {
		
		if (direccion == -1) {
			SenFrente.setPosition(this.getX() - 15, this.getY() + 10);
		} else {
			SenFrente.setPosition(this.getX() + this.getWidth() + 15 - this.getWidth() / 6, this.getY() + 10);
		}

	}

	/**
	 * Se colocan los sensores en su sitio segun 
	 * la direccione que lleve
	 */
	private void ajustarEnSuelo() {// Se colocan los sensores en su sitio segun
		// �a direccione que lleve
		if (direccion == -1) {
			SenSuelo.setPosition(this.getX() + 15, this.getY() - 20);
		} else {
			SenSuelo.setPosition(this.getX() + this.getWidth() - 15 - this.getWidth() / 6, this.getY() - 20);
		}

	}		
}
