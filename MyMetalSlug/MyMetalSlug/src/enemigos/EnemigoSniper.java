package enemigos;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;
import unJugador.PantallaDeJuego;

public class EnemigoSniper extends BaseActor {
	// De esta clase eredan todos los enemigos

	public boolean vivo;// Vivo o no el enemigo
	protected PantallaDeJuego nivel;// Saber que nivel es
	int vidaEnemigo;// Cantidad de vida del enemigo

	public EnemigoSniper(float x, float y, Stage s, PantallaDeJuego nivel) {
		super(x, y, s);

		this.nivel = nivel;// Indicamos del nivel que es
		vivo = true;// Indicamos que el enemigo esta vivo
		vidaEnemigo = 40;// Configuramos que el enemigo tiene 20 de vida
	}

	/**
	 * Da�o que recibe el enemigo, se descuenta del contador de vida 
	 * y si es menor o igual que cero se indica que el enemigo esta muerto
	 * @param dano
	 */
	public void dano(int dano) {// Da�o que recibe el enemigo

		this.vidaEnemigo -= dano;// Calculamos la vida que le queda al enemigo

		if (vidaEnemigo >= 1) {
			this.vivo = true;// Si el enemigo sigue con vida el enemigo sigue en
								// la pantalla
		} else {
			this.vivo = false; // Si muere lo desactivamos
		}

	}

	/**
	 * Si esta vivo se pinta el enemigo
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (vivo) {
			super.draw(batch, parentAlpha);// Pintamos al enemigo si esta vivo
		}
	}

}
