package enemigos;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;
import unJugador.PantallaDeJuego;

public class Sniper extends EnemigoSniper {
	Animation andando;
	
	boolean viendo;// Indica si esta viendo a jugador

	private float tiempoComportamiento;// Tiempo de persecucion
	private float cuentaComportamiento;// Cuenta el tiempo

	private float aceleracion;// Declaro la aceleracion
	private float velocidadMaxima;// Declaro la velocidad maxima
	private int numero;// Variable para un switch case
	private float distanciaVision;// La distancio en la que ve a jugador
	private float inicioX;// Donde empieza
	private float inicioY;// Donde empieza
	private BaseActor SenSuelo;// Indicar los sensores del saltarin del suelo

	// Igual que Fantas, las variables para el disparo
	private float tiempoDisparo;
	private float cuentaDisparo;
	private ArrayList<BalaEnemigos> balas;
	private int numbalas;
	private int balaActual;
	
	
	/**
	 * En este metodo iniciamos todas las variables del jugador 2
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public Sniper(float x, float y, Stage s, PantallaDeJuego nivel) {
		super(x, y, s, nivel);

		this.inicioX = x;// Indicamos el comiezo
		this.inicioY = y;// Indicamos el comiezo
		viendo = false;
		String[] walkFileNames2 = { "assets/Enemi/tile000.png", "assets/Enemi/tile001.png",
				"assets/Enemi/tile002.png", "assets/Enemi/tile003.png", "assets/Enemi/tile004.png", "assets/Enemi/tile005.png","assets/Enemi/tile006.png","assets/Enemi/tile007.png",
				"assets/Enemi/tile008.png","assets/Enemi/tile009.png","assets/Enemi/tile010.png","assets/Enemi/tile011.png","assets/Enemi/tile012.png"};
		andando = loadAnimationFromFiles(walkFileNames2, 0.1f, true);
		
		velocidadMaxima = 50;// Velocidad maxima
		aceleracion = 200;// La aceleeracion
		this.cuentaComportamiento = 0;// Variables para el comportammiento del
										// helicoptero
		this.tiempoComportamiento = 0.5f;
		distanciaVision = 420;// La distancia de vision
		numbalas = 5;// Balas que puede tener en pantalla a la vez
		balaActual = 0;// Balas que hay en pantalla
		balas = new ArrayList<BalaEnemigos>();// Array de balas
		BalaEnemigos bala;
		this.tiempoDisparo = 1;// Tiempo entre disparo
		this.cuentaDisparo = 0;
		
		// Creamos los dos sensores
		SenSuelo = new BaseActor(0, 0, s);
		SenSuelo.setSize(this.getWidth() / 6, 30);
		SenSuelo.setBoundaryRectangle();
		

		for (int i = 0; i < numbalas; i++) {
			bala = new BalaEnemigos(0, 0, s, nivel);
			balas.add(bala);// A�adir balas al array
		}
	

	

	}

	/**
	 * Indico en tiempo real como debe de actual el helicoptero en cada momento
	 */
	@Override
	public void act(float dt) {

		super.act(dt);
		
		

		if (this.vivo) {// Si esta vivp
			if (this.viendo) {// Si esta viendo
				
				if (this.cuentaDisparo <= 0) {
					disparar();// Y aqui hago el disparo
				} else {
					cuentaDisparo -= dt;
				}
				
				if (Math.sqrt(Math.pow(this.getX() - nivel.jugador.getX(), 2)
						+ Math.pow(this.getY() - nivel.jugador.getY(), 2)) > distanciaVision) {
					viendo = false;// Controlamos la distancia de vision
										// para ver cuando poner a perseguir
				}
			} else {// Si no esta viendo
				if (Math.sqrt(Math.pow(this.getX() - nivel.jugador.getX(), 2)
						+ Math.pow(this.getY() - nivel.jugador.getY(), 2)) < distanciaVision) {
					viendo = true;// Controlamos la distancia de vision
										// para ver cuando poner a perseguir
				} 
			}

			

			accelerationVec.set(0, 0);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);
		}
		
		if (nivel.jugador.getX() < this.getX()) {// Si se da de frente cambia de direccion
			setScaleX(1);
		}else {
			setScaleX(-1);
		}
		
		boundToWorld();
	
	}
	
	
	/**
	 * dispara las balas
	 */
	private void disparar() {
		this.cuentaDisparo = this.tiempoDisparo;

		float x = this.getX() - nivel.jugador.getX();
		float y = this.getY() - nivel.jugador.getY();// Para ver direccion de
														// jugador para
														// dispararle
		Vector2 vector = new Vector2(x, y).nor();
		balas.get(balaActual).disparar(vector.x * -1, vector.y * -1, this.getX() + 60, this.getY() + 10);// Actualiza
																											// las
																											// variables
		balaActual++;
		balaActual = balaActual % numbalas;

	}

}
