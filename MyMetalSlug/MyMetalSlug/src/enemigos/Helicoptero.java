package enemigos;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import unJugador.PantallaDeJuego;

public class Helicoptero extends EnemigoHelicoptero {
	// Enemigo que persigue al jugador cuando este se acerca y le dispara
	Animation andando;

	boolean persiguiendo;// Indica si esta persiguiendo a Rossi

	private float tiempoComportamiento;// Tiempo de persecucion
	private float cuentaComportamiento;// Cuenta el tiempo

	private float aceleracion;// Declaro la aceleracion
	private float velocidadMaxima;// Declaro la velocidad maxima
	private int numero;// Variable para un switch case
	private float distanciaVision;// La distancio en la que ve a Rossi
	private float inicioX;// Donde empieza
	private float inicioY;// Donde empieza

	// Igual que Fantas, las variables para el disparo
	private float tiempoDisparo;
	private float cuentaDisparo;
	private ArrayList<BalaHelicoptero> balas;
	private int numbalas;
	private int balaActual;

	
	/**
	 * En este metodo iniciamos todas las variables del jugador 2
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public Helicoptero(float x, float y, Stage s, PantallaDeJuego nivel) {

		super(x, y, s, nivel);
		this.inicioX = x;// Indicamos el comiezo
		this.inicioY = y;// Indicamos el comiezo
		persiguiendo = false;
		String[] walkFileNames = { "assets/Enemi/h1.png", "assets/Enemi/h2.png", "assets/Enemi/h3.png",
				"assets/Enemi/h4.png" };// Cargamos las imagenes para la
											// animacion
		andando = loadAnimationFromFiles(walkFileNames, 0.1f, true);// Hacemos la animacion
		velocidadMaxima = 150;// Velocidad maxima
		aceleracion = 500;// La aceleeracion
		this.cuentaComportamiento = 0;// Variables para el comportammiento del helicoptero
		this.tiempoComportamiento = 0.5f;
		distanciaVision = 600;// La distancia de vision
		numbalas = 10;// Balas que puede tener en pantalla a la vez
		balaActual = 0;// Balas que hay en pantalla
		balas = new ArrayList<BalaHelicoptero>();// Array de balas
		BalaHelicoptero bala;
		this.tiempoDisparo = 1;// Tiempo entre disparo
		this.cuentaDisparo = 0;

		for (int i = 0; i < numbalas; i++) {
			bala = new BalaHelicoptero(0, 0, s, nivel);
			balas.add(bala);// Añadir balas al array
		}
	}

	/**
	 * Indico en tiempo real como debe de actual el helicoptero en cada momento
	 */
	@Override
	public void act(float dt) {
		super.act(dt);

		if (this.vivo) {// Si esta vivp
			if (this.persiguiendo) {// Si esta persiguiendo
				perseguir(nivel.jugador.getX(), nivel.jugador.getY(), false);// Hacemos el metodo de perseguir
				if (this.cuentaDisparo <= 0) {
					disparar();// Y aqui hago el disparo
				} else {
					cuentaDisparo -= dt;
				}
			} else {// Si no esta persiguiendo
				if (Math.sqrt(Math.pow(this.getX() - nivel.jugador.getX(), 2)
						+ Math.pow(this.getY() - nivel.jugador.getY(), 2)) < distanciaVision) {
					persiguiendo = true;// Controlamos la distancia de vision
										// para ver cuando poner a perseguir
				} else {
					if (this.cuentaComportamiento <= 0) {
						numero = (int) Math.floor(Math.random() * 6 + 1);
						cuentaComportamiento = this.tiempoComportamiento;
					} else {
						cuentaComportamiento -= dt;
					} // Hace que se mueva el helicopterio mientras espera ver a Rossi
					

					
				}
			}

			// Movimiento del helicoptero
			velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
			velocityVec.x = MathUtils.clamp(velocityVec.x, -velocidadMaxima, velocidadMaxima);
			velocityVec.y = MathUtils.clamp(velocityVec.y, -velocidadMaxima, velocidadMaxima);

			accelerationVec.set(0, 0);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);
		}
		boundToWorld();
	}

	/**
	 * metodo para perseguir al jugador
	 * @param x
	 * @param y
	 * @param accurate
	 */
	private void perseguir(float x, float y, boolean accurate) {
		float margen = 0;
		if (!accurate) {
			margen = 100;
		}

		if (x + margen < this.getX()) {
			accelerationVec.add(-aceleracion, 0);

		} else if (x - margen > this.getX()) {
			accelerationVec.add(aceleracion, 0);
		}
		
	}

	/**
	 * dispara las balas
	 */
	private void disparar() {
		this.cuentaDisparo = this.tiempoDisparo;

		float x = 0;
		float y = this.getX();
		Vector2 vector = new Vector2(x, y).nor();
		balas.get(balaActual).disparar(vector.x * -1, vector.y * -1, this.getX() + 60, this.getY());
		balaActual++;
		balaActual = balaActual % numbalas;

	}
}
