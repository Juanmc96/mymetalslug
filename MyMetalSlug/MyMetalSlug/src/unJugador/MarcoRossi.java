package unJugador;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;
import metalslug.Cuesta;
import metalslug.Fin;
import metalslug.Inmortal;
import metalslug.JuegoMyMetalSlug;
import metalslug.MasBalas;
import metalslug.Parametros;
import metalslug.Portal;
import metalslug.Revive;
import metalslug.Solid;

public class MarcoRossi extends BaseActor {

	private float aceleracionAndando;// Aceleracion del personaje
	private float velocidadMaximaAndando;// Velocidad maxima
	private float velocidadSalto;// Velocidad de salto

	public float vida;// vida restante

	// Sensor para el salto
	private float alturaPies;
	private float offsetPies;
	private BaseActor pies;// sensor de los pies

	// Animaciones del personaje
	private Animation parado;
	private Animation andando;
	private Animation saltando;
	private Animation dispWalk;
	private Animation shoot;
	private Animation upShoot;
	private Animation upShootJump;
	private Animation shootJump;
	private Animation walkUpShoot;

	private ArrayList<Bala> balas; // Array de las balas
	private int numBalas;// n�mero total de balas
	private float coolDownBala;// tiempo entre disparos
	private float cuentaCoolDownBala;// llevar la cuenta entre disparos
	private int balaActual;// Indica la siguiente bala

	// gesti�n de invulnerabilidad
	private float tiempoInvulnerable; // Tiempo sin tener da�o
	private float cuentaInvulnerable;// cuenta para el tiempo sin da�o

	// referencia al nivel actual para poder acceder a sus elementos
	private PantallaDeJuego nivel;

	private int direDisp = 1;

	/**
	 * En este metodo iniciamos todas las variables del jugador 1
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public MarcoRossi(float x, float y, Stage s, PantallaDeJuego nivel) {
		super(x, y, s);

		this.nivel = nivel;
		// Animaciones
		String[] stopFileNames = { "assets/Player1/frame_0_delay-0.08s.gif",
				"assets/Player1/frame_1_delay-0.08s.gif",
				"assets/Player1/frame_2_delay-0.08s.gif",
				"assets/Player1/frame_3_delay-0.08s.gif",
				"assets/Player1/frame_4_delay-0.08s.gif" };
		parado = loadAnimationFromFiles(stopFileNames, 0.08f, true);
		String[] walkFileNames = { "assets/Player1/0000.png",
				"assets/Player1/0001.png",
				"assets/Player1/0002.png",
				"assets/Player1/0003.png",
				"assets/Player1/0004.png",
				"assets/Player1/0005.png",
				"assets/Player1/0006.png",
				"assets/Player1/0007.png",
				"assets/Player1/0008.png",
				"assets/Player1/0009.png",
				"assets/Player1/0010.png",
				"assets/Player1/0011.png",
				"assets/Player1/0012.png",
				"assets/Player1/0013.png",
				"assets/Player1/0014.png" };
		andando = loadAnimationFromFiles(walkFileNames, 0.08f, true);
		String[] jumpFileNames = { "assets/Player1/0.png",
				"assets/Player1/1.png", "assets/Player1/2.png",
				"assets/Player1/3.png", "assets/Player1/4.png",
				"assets/Player1/5.png", };
		saltando = loadAnimationFromFiles(jumpFileNames, 0.08f, true);
		String[] dispWalkFileNames = { "assets/Player1/ws1.png","assets/Player1/ws2.png",
				"assets/Player1/ws3.png","assets/Player1/ws4.png",
				"assets/Player1/ws5.png"};
		dispWalk = loadAnimationFromFiles(dispWalkFileNames, 0.08f, true);
		String[] shootFileNames = { "assets/Player1/shoot.png"};
		shoot = loadAnimationFromFiles(shootFileNames, 0.08f, true);
		String[] upShootFileNames = { "assets/Player1/up6.png"};
		upShoot = loadAnimationFromFiles(upShootFileNames, 0.08f, true);
		String[] upShootJumpFileNames = { "assets/Player1/ju0.png"};
		upShootJump = loadAnimationFromFiles(upShootJumpFileNames, 0.08f, true);
		String[] shootJumpFileNames = { "assets/Player1/js.png"};
		shootJump = loadAnimationFromFiles(shootJumpFileNames, 0.08f, true);
		String[] walkUpShootFileNames = { "assets/Player1/wu0.png","assets/Player1/wu1.png",
				"assets/Player1/wu2.png","assets/Player1/wu3.png",
				"assets/Player1/wu4.png","assets/Player1/wu5.png",
				"assets/Player1/wu6.png","assets/Player1/wu7.png",};
		walkUpShoot = loadAnimationFromFiles(walkUpShootFileNames, 0.08f, true);

		// Colisi�n del personaje
		setBoundaryPolygon(10);

		// Valor de los distintos atributos
		velocidadMaximaAndando = 130;
		aceleracionAndando = 1000;
		velocidadSalto = 360;
		alturaPies = 2;
		offsetPies = 10;
		vida = 100;
		tiempoInvulnerable = 1;
		cuentaInvulnerable = 0;

		balaActual = 0;
		numBalas = 60;
		this.coolDownBala = 0.3f;
		this.cuentaCoolDownBala = 0;
		// Inicializaci�n array de balas
		balas = new ArrayList<Bala>();
		// creaci�n de las balas
		for (int i = 0; i < numBalas; i++) {
			balas.add(new Bala(0, 0, s, nivel));
		}

		// Creaci�n del sensor de salto
		pies = new BaseActor(0, 0, s);
		pies.setSize(this.getWidth() - offsetPies, alturaPies);
		pies.setBoundaryRectangle();

	}

	/**
	 * En este metodo ejecutamos todos los metodos en tiempo real, como es controlar las pulsaciones de teclado
	 * si esta tocando el personaje un salido y ponerle la animacion que tiene que llevar en cada momento
	 */
	public void act(float dt) {

		super.act(dt);

		if (this.cuentaInvulnerable > 0) {// si el personaje es invulnerable, descontamos tiempo
			this.cuentaInvulnerable -= dt;
		}

		if ((Gdx.input.isKeyPressed(Keys.SPACE))) {
			if (this.isOnSolid()) {// Si esta sobre solido salta
				this.salto();
			}
		}

		if (Gdx.input.isKeyPressed(Keys.W)) {
			direDisp = 2;// Decide hacia donde se dispara
		}

		if ((Gdx.input.isKeyPressed(Keys.Q)) && this.cuentaCoolDownBala <= 0) {
			numBalas = 60;

			// Segun la direccion del personaje dispara en una direccion u otra
			if (direDisp == 0) {
				this.disparar(-1);
			} else if (direDisp == 1) {
				this.disparar(1);
			} else {
				this.disparar2(2);
			}
		}

		if (cuentaCoolDownBala > 0) {// Si a�n no se ha terminado el tiempo de
			// coolDown de disparo, se decrementa
			this.cuentaCoolDownBala -= dt;
		}

		if ((!Gdx.input.isKeyPressed(Keys.Q))) {
			numBalas = 0;
		}

		if (Gdx.input.isKeyPressed(Keys.D)) {

			accelerationVec.add(aceleracionAndando, 0);
			setScaleX(1);// Al pulsar sobre la D se mueve hacia la derecha y la imagen se pone en mirando
							// a la derecha
			direDisp = 1;// Decide hacia donde se dispara

			if (JuegoMyMetalSlug.jug == 2) {
				if (this.getX() - nivel.jugador2.getX() > 221) {
					velocidadMaximaAndando = 0;
				} else {
					velocidadMaximaAndando = 130;
				}
			}
		}
		if (Gdx.input.isKeyPressed(Keys.A)) {

			accelerationVec.add(-aceleracionAndando, 0);
			setScaleX(-1);// Al pulsar sobre la D se mueve hacia la izquierda y la imagen se pone en
							// mirando a la izquierda
			direDisp = 0;// Decide hacia donde se dispara

			if (JuegoMyMetalSlug.jug == 2) {
				if (this.getX() - nivel.jugador2.getX() < -221) {
					velocidadMaximaAndando = 0;
				} else {
					velocidadMaximaAndando = 130;
				}
			}
		}

		if (!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.D)) {
			velocityVec.x = 0;

		}

		if (Gdx.input.isKeyPressed(Keys.A) && Gdx.input.isKeyPressed(Keys.D)) {
			velocityVec.x = 0;

		}

		// Aplicamos la gravedad al vector de aceleraci�n
		accelerationVec.add(0, -Parametros.getGravedad());

		// Calculamos la velocidad a partir de la aceleraci�n
		velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		velocityVec.x = MathUtils.clamp(velocityVec.x, -velocidadMaximaAndando, velocidadMaximaAndando);

		// Calculamos el movimiento
		moveBy(velocityVec.x * dt, velocityVec.y * dt);
		// reiniciamos la aceleraci�n
		accelerationVec.set(0, 0);

		// actualizamos la posici�n del sensor de los pies
		pies.setPosition(this.getX() + offsetPies / 2, this.getY() - alturaPies);

		// gestionamos la animaci�n actual seg�n el estado del personaje
		if (this.isOnSolid()) {
			if (velocityVec.x == 0) {
				if(Gdx.input.isKeyPressed(Keys.W) && Gdx.input.isKeyPressed(Keys.Q)) {
					setAnimation(upShoot);
				}
				else if(direDisp==2 && Gdx.input.isKeyPressed(Keys.Q)) {
					setAnimation(upShoot);
				}
				else if(Gdx.input.isKeyPressed(Keys.Q)) {
					setAnimation(shoot);
				}
				else if(Gdx.input.isKeyPressed(Keys.W)) {
					setAnimation(upShoot);
				}
				else {
					setAnimation(parado);
				}
			} else {
				if(Gdx.input.isKeyPressed(Keys.W) ) {
					setAnimation(walkUpShoot);
				}
				else if(Gdx.input.isKeyPressed(Keys.Q)) {
					setAnimation(dispWalk);
				}else {
					setAnimation(andando);
				}
			}

		} else {
			if(Gdx.input.isKeyPressed(Keys.Q) && Gdx.input.isKeyPressed(Keys.W)) {
				setAnimation(upShootJump);
			}else if(Gdx.input.isKeyPressed(Keys.Q)) {
				setAnimation(shootJump);
			}
			else {
				setAnimation(saltando);
			}
		}

		// alinear la c�mara
		alignCamera();
		// impedir que se salga del mundo
		boundToWorld();
	}

	/**
	 * Comprueba si esta sobre solido
	 * @return
	 */
	public boolean isOnSolid() {
		// devuelve 'true' si el personaje est� sobre un s�lido activo
		for (Solid solido : nivel.getSolidos()) {
			if (this.pies.overlaps(solido) && solido.isEnabled()) {
				return true;
			}

		}
		for (Cuesta cuesta : nivel.getCuestas()) {
			if (this.pies.overlaps(cuesta)) {
				return true;
			}

		}
		return false;

	}

	/**
	 * recive da�o
	 * @param dano
	 */
	public void dano(int dano) {// Da�o a Rossi
		if (this.cuentaInvulnerable <= 0) {
			this.vida -= dano;
			this.cuentaInvulnerable = this.tiempoInvulnerable;
		}
	}

	/**
	 * Funcion para saltar
	 */
	public void salto() {
		// funci�n de salto
		velocityVec.y = velocidadSalto;
		nivel.JumpSound();// Activa el sonido del salto
	}

	/**
	 * Si esta saltando
	 * @return
	 */
	public boolean isJumping() {
		return (velocityVec.y > 0);
	}

	/**
	 * Si esta cayendo
	 * @return
	 */
	public boolean isFalling() {
		return (velocityVec.y < 0);
	}

	/**
	 * Creas el disparo indicando la direccion que debe de llevar
	 * @param direccion
	 */
	public void disparar(int direccion) {
		// Para disparar las balas
		balas.get(balaActual).disparar(direccion);
		balaActual++;
		balaActual = balaActual % numBalas;
		cuentaCoolDownBala = coolDownBala;
		nivel.DispSound();// Sonido del disparo

	}

	/**
	 * Creas el disparo indicando la direccion que debe de llevar
	 * @param direccion
	 */
	public void disparar2(int direccion) {
		// Para disparar las balas
		balas.get(balaActual).disparar2(direccion);
		balaActual++;
		balaActual = balaActual % numBalas;
		cuentaCoolDownBala = coolDownBala;
		nivel.DispSound();// Sonido del disparo
	}

	/**
	 * Cantidad de balas
	 * @param balas
	 */
	public void balas(float balas) {

		this.coolDownBala = balas;

	}

	/**
	 * Cantidad de vida
	 * @param balas
	 */
	public void vida(float vida) {

		this.vida = vida;

	}

	/**
	 * Comprueba si has cogido el objeto
	 * @return
	 */
	public boolean isOnBalas() {
		for (MasBalas solido : nivel.masbalas) {
			if (this.pies.overlaps(solido)) {
				return true;
			}

		}
		return false;

	}

	/**
	 * Comprueba si has cogido el objeto
	 * @return
	 */
	public boolean isOnInmortal() {
		for (Inmortal solido : nivel.inmortal) {
			if (this.pies.overlaps(solido)) {
				return true;
			}

		}
		return false;

	}

	/**
	 * Comprueba si has cogido el objeto
	 * @return
	 */
	public boolean isOnRevive() {
		for (Revive solido : nivel.revive) {
			if (this.pies.overlaps(solido)) {
				return true;
			}

		}
		return false;

	}

	/**
	 * Comprueba si has entrado en el portal de siguiente mapa
	 * @return
	 */
	public boolean isOnPortal() {
		for (Portal solido : nivel.portal) {
			if (this.pies.overlaps(solido)) {
				return true;
			}

		}
		return false;

	}
	
	/**
	 * Comprueba si has entrado en el portal de fin de juego
	 * @return
	 */
	public boolean isOnFin() {
		for (Fin solido : nivel.fin) {
			if (this.pies.overlaps(solido)) {
				return true;
			}

		}
		return false;

	}

}
