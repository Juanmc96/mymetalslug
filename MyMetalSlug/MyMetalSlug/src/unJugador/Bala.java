package unJugador;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;
import enemigos.EnemigoCuchillo;
import enemigos.EnemigoHelicoptero;
import enemigos.EnemigoSniper;
import metalslug.Solid;

public class Bala extends BaseActor {
	// Proyectil disparado por el jugador
	private PantallaDeJuego nivel;

	// Propiedades de la bala
	private float tiempoBala; // Duracion de la bala
	private float velocidadBala; // velocidad de bala
	private float cuentaBala;// Llva la cuenta de tiempo de la bala
	private int direccion;// hacia donde va la bala

	private boolean enabled;
	
	/**
	 * inicializo las variables
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public Bala(float x, float y, Stage s, PantallaDeJuego nivel) {
		super(x, y, s);

		this.nivel = nivel;
		// Iniciamos las variables
		tiempoBala = (float) 0.87;
		velocidadBala = 500;

		// Imagen de la bala
		loadTexture("assets/extra/bala.png");
	}

	/**
	 * Posicion de la bala en tiempo real y las colisiones con los enemigos en tiempo real
	 */
	@Override
	public void act(float dt) {

		if (enabled) {// Bala activa o no
			super.act(dt);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);// Posici�n de la bala seg�n su velocidad
			cuentaBala -= dt; // actualizamos el tiempo que le queda a la bala

			for (EnemigoCuchillo enemigoCu : nivel.enemigosCuchillo) {// Comprobar si bala
													// colisiona con enemigo
				if (enemigoCu.vivo && this.enabled && this.overlaps(enemigoCu)) {
					enemigoCu.dano(10);// Da�o a jugador
					this.enabled = false; // Quitamos la bala cuando impacta con jugador
				}
				if (cuentaBala <= 0) {// Si se ha terminado el tiempo en pantalla desactivo la bala
					enabled = false;
				}
			}
			
			for (EnemigoHelicoptero enemigoHeli : nivel.enemigosHelicoptero) {// Comprobar si bala
				// colisiona con enemigo
				if (enemigoHeli.vivo && this.enabled && this.overlaps(enemigoHeli)) {
					enemigoHeli.dano(10);// Da�o a jugador
					this.enabled = false; // Quitamos la bala cuando impacta con jugador
				}
				if (cuentaBala <= 0) {// Si se ha terminado el tiempo en pantalla desactivo la bala
					enabled = false;
				}
			}
			
			for (EnemigoSniper enemigoSni : nivel.enemigosSniper) {// Comprobar si bala
				// colisiona con enemigo
				if (enemigoSni.vivo && this.enabled && this.overlaps(enemigoSni)) {
					enemigoSni.dano(10);// Da�o a jugador
					this.enabled = false; // Quitamos la bala cuando impacta con jugador
				}
				if (cuentaBala <= 0) {// Si se ha terminado el tiempo en pantalla desactivo la bala
					enabled = false;
				}
			}
		}
	}

	/**
	 * Pinto la bala si esta activa
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (enabled) {// Mostrar la bala si el disparo esta activo
			super.draw(batch, parentAlpha);
		}
	}

	/**
	 * Metodo que indica comienzo y direccion de la bala
	 * @param direccion
	 */
	public void disparar(int direccion) {
		// Al disparar se activa:

		this.enabled = true;// Activo la bala
		this.setPosition(nivel.jugador.getX() + 20, nivel.jugador.getY() +20);// Donde comienza el disparo
		cuentaBala = tiempoBala;// Inicio la cuenta de la bala
		this.velocityVec.set(velocidadBala * direccion, 0);// Velocidad positiva o negativa segun la direccion de la bala
	}

	/**
	 * Metodo que indica comienzo y direccion de la bala
	 * @param direccion
	 */
	public void disparar2(int direccion) {
		// Al disparar se activa:

				this.enabled = true;// Activo la bala
				this.setPosition(nivel.jugador.getX() + 14, nivel.jugador.getY() + 40);// Donde comienza el disparo
				cuentaBala = tiempoBala;// Inicio la cuenta de la bala
				this.velocityVec.set(0, velocidadBala * direccion);// Velocidad positiva o negativa segun la direccion de la bala
	}
}
