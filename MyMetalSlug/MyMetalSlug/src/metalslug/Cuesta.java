package metalslug;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.Stage;
import base.BaseActor;

public class Cuesta extends BaseActor {
	
	private boolean enabled;
	
	/**
	 * Posicion de las cuestas
	 * @param x
	 * @param y
	 * @param s
	 * @param poly
	 */
	public Cuesta(float x, float y, Stage s, Polygon poly) {
		super(x, y, s);

		this.boundaryPolygon=poly;

	}
	
	/**
	 * Poner las cuestas como enables
	 * @return
	 */
	public boolean isEnabled() {
		return enabled;
	}

}
