package metalslug;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Suelo extends Solid {
	ShapeRenderer shapeRenderer;

	public Suelo(float x, float y, float width, float height, Stage s) {
		super(x, y, width, height, s);

		shapeRenderer = new ShapeRenderer();

	}

}
