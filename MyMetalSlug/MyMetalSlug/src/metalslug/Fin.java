package metalslug;

import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;

public class Fin extends BaseActor {

	public Fin(float x, float y, Stage s) {
		super(x, y, s);
		loadTexture("assets/items/portal.png");// Imagen del portal
	}

}
