package metalslug;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class Launcher {
	public static void main(String[] args) {
		Game myGame = new JuegoMyMetalSlug();
		LwjglApplication launcher = new LwjglApplication(myGame, "MetalSlug", Parametros.anchura, Parametros.altura);
	}
}