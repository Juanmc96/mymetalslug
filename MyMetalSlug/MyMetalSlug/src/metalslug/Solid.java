package metalslug;

import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;

public class Solid extends BaseActor {
	private boolean enabled;
 /**
  * Posicion de los solidos
  * @param x
  * @param y
  * @param width
  * @param height
  * @param s
  */
	public Solid(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		setSize(width, height);
		setBoundaryRectangle();
		enabled = true;
	}
	
	/**
	 * Indica que esta en enable
	 * @return
	 */
	public boolean isEnabled() {
		return enabled;
	}
}