package metalslug;


import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;

public class Revive extends BaseActor {

	public Revive(float x, float y, Stage s) {
		super(x, y, s);

		loadTexture("assets/items/up.jpg");// Cargar imagen del powerup de
												// la bala
	}

}
