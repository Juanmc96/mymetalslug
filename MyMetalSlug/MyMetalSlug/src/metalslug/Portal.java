package metalslug;

import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;

public class Portal extends BaseActor {

	public Portal(float x, float y, Stage s) {
		super(x, y, s);
		loadTexture("assets/items/portal.png");//Imagen del portal
	}

}
