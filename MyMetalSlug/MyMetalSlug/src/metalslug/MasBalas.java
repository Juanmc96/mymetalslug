package metalslug;

import com.badlogic.gdx.scenes.scene2d.Stage;

import base.BaseActor;

public class MasBalas extends BaseActor {

	public MasBalas(float x, float y, Stage s) {
		super(x, y, s);

		loadTexture("assets/items/balapup.png");// Cargar imagen del powerup de
												// la bala
	}

}
