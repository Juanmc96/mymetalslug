package metalslug;

public class Parametros {
	// Par�metros globlales accesibles para todas las clases
	private static float gravedad = 900;//Gravedad del juego
	public static int anchura = 1800;//Tama�o de la pantalla
	public static int altura = 820;//Tama�o de la pantalla
	public static int nivel = 1;
	public static boolean fin = true; // si el juego acaba y fin= true significa
										// que ha terminado por muerte

	public static float getGravedad() {
		return gravedad;
	}

	public static void setGravedad(float gravedad) {
		Parametros.gravedad = gravedad;
	}

}
