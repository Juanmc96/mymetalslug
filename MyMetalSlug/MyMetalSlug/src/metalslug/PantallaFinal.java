package metalslug;

import java.io.File;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import base.BaseGame;
import base.BaseScreen;

public class PantallaFinal extends BaseScreen {
	private Label mensaje;
	private Music theme;
	private float volumen;

	/**
	 * Inicializo la musica y los label
	 */
	@Override
	public void initialize() {
		
		theme = Gdx.audio.newMusic(Gdx.files.internal("assets/Sound/menu.mp3"));
		
		// Volumen de los sonidos
				String linea = null;

				File fichero = new File("Configuracion.txt");
				Scanner s = null;

				try {
					s = new Scanner(fichero);
					linea = s.nextLine(); // Guardamos la linea en un String

				} catch (Exception ex) {
					System.out.println("Mensaje: " + ex.getMessage());
				} finally {
					// Cerramos el fichero tanto si la lectura ha sido correcta o no
					try {
						if (s != null)
							s.close();
					} catch (Exception ex2) {
						System.out.println("Mensaje 2: " + ex2.getMessage());
					}
				}

				if (linea.equals("1")) {
					volumen = 0.2f;
				} else {
					volumen = 0;
				}

				theme.setLooping(true);
				theme.setVolume(volumen);
				theme.play();// Inicio los sonidos
		
		((OrthographicCamera) this.mainStage.getCamera()).setToOrtho(false, Parametros.anchura * 2,Parametros.altura * 2);
		FondoFinal fondoInicio = new FondoFinal(0, 0, backStage, this);
		mensaje = new Label("", BaseGame.labelStyle);
		if (!Parametros.fin) {
			mensaje.setText("Has ganado, pulsa ESC para volver a jugar");// Mensaje si ganas la partida
		} else {
			mensaje.setText("Has perdido, pulsa ESC para volver a jugar");// Mesaje si pierdes la partida

		}

		uiStage.addActor(mensaje);
		mensaje.setPosition(430, 600);// Posicion del mensaje
	}

	/**
	 * Si pulso escape me lleva a la pantalla inicio
	 */
	@Override
	public void update(float dt) {
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {// Si pulsas escape
			this.theme.dispose();// Desactiva la musica											// vuelves a jugar
			this.dispose();

			BaseGame.setActiveScreen(new PantallaInicio());
		}

	}

}
